package org.pwc.assignment.dao.exception;

/**
 * @author Nader Albert
 * @since 7/18/2014.
 */
public class DataAccessException extends Exception{
  public DataAccessException(String errorMessage) {
    super (errorMessage);
  }
}
