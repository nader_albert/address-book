package org.pwc.assignment.dao.impl;

import org.pwc.assignment.dao.AddressBookDao;
import org.pwc.assignment.dao.exception.DataAccessException;
import org.pwc.assignment.model.entity.AddressBook;
import org.pwc.assignment.model.entity.User;

import java.io.*;
import java.nio.file.*;

/**
 * A data access object implementation relying on the File System as the persistent backend storage.
 * uses java serialization to persist java objects, which makes it a 'not very flexible' implementation, as non-java
 * clients will not be able to deal with the data persisted. Other possible implementations could also be provided
 * @see AddressBookMongoDao as an example sample that can rely on MongoDB. It is not implemented though, but it is added
 * just to show the concept.
 *
 * @author Nader Albert
 * @since 7/18/2014.
 **/
public class AddressBookFileSystemDao implements AddressBookDao{

  @Override
  public boolean saveAddressBook(AddressBook book) throws DataAccessException{
    FileOutputStream fileOutput;
    ObjectOutputStream objectOutput;

    try {
      fileOutput = new FileOutputStream(composeFileName(book.getOwner()));
    } catch (FileNotFoundException fex){
      throw new DataAccessException("object file couldn't be located on disk"
              + (fex.getMessage()==null?"" : fex.getMessage()));
    }

    try {
      objectOutput = new ObjectOutputStream(fileOutput);
    } catch (IOException iox){
      throw new DataAccessException("couldn't read from the provided input stream"
              + (iox.getMessage()==null?"" : iox.getMessage()));
    }

    try {
      objectOutput.writeObject(book);
    } catch (IOException iox) {
      throw new DataAccessException("couldn't write address book to disk"
              + (iox.getMessage() == null ? "" : iox.getMessage()));
    }
    try {
      fileOutput.close();
    } catch (IOException iox) {
      throw new DataAccessException("an exception has been caught while closing the file"
              + (iox.getMessage() == null ? "" : iox.getMessage()));
    }

    return true;
  }

  @Override
  public AddressBook retrieveAddressBook(User user) throws DataAccessException{

    FileInputStream fileInput;
    ObjectInputStream objectInput;
    Object retrievedObject;
    try {
      fileInput = new FileInputStream(composeFileName(user));
    } catch (FileNotFoundException fex){
      throw new DataAccessException(" object file couldn't be located on disk "
              + (fex.getMessage()==null?"" : fex.getMessage()));
    }
    try {
      objectInput = new ObjectInputStream(fileInput);
    } catch (IOException iox){
      throw new DataAccessException("couldn't read from the provided input stream"
              + (iox.getMessage()==null?"" : iox.getMessage()));
    }
    try {
      retrievedObject = objectInput.readObject();
    } catch (IOException iox) {
      throw new DataAccessException("couldn't locate a corresponding class for the de-serialized object"
              + (iox.getMessage()==null?"" : iox.getMessage()));
    } catch (ClassNotFoundException cnf) {
      throw new DataAccessException("couldn't locate a corresponding class for the de-serialized object"
              + (cnf.getMessage()==null ? "" : cnf.getMessage()));
    }

    if (retrievedObject instanceof AddressBook)
      return (AddressBook) retrievedObject;

    return null;
  }

  public boolean deleteAddressBook(User user)throws DataAccessException{
    try {
      Files.deleteIfExists(Paths.get((composeFileName(user))));
    } catch (IOException iox) {
      throw new DataAccessException("couldn't locate a corresponding class for the de-serialized object"
              + (iox.getMessage()==null ? "" : iox.getMessage()));
    }
    return true;
  }

  /**
   * @return true, if the given user has an a corresponding AddressBook in the backend storage, false otherwise
   * */
  public boolean exists(User user) {
    //PathMatcher matcher =
    //        FileSystems.getDefault().getPathMatcher((composeFileName(user)));
    //Files.
    //return (matcher.matches(Paths.get((composeFileName(user)))));
    try {
      new FileInputStream(composeFileName(user));
    } catch (FileNotFoundException fex){
      return false;
    }
    return true;
  }

  /**
   * A utility method used by the save, update, and retrieve methods to recognize the exact file, wrapping the required
   * AddressBook
   * composes a file name for a serialized version of an Address Book to be persisted on Disk, that considers the name of
   * the address book owner.
   * @return a String representing the composed name for an AddressBook file, that takes into consideration the name of the
   * owner of this Address Book
   * */
  private String composeFileName(User user){
    return /*"..\\data\\" +*/ "addressbook_"+ user.getName()+".data";
  }
}
