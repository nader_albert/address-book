package org.pwc.assignment.dao.impl;

import org.pwc.assignment.dao.AddressBookDao;
import org.pwc.assignment.model.entity.AddressBook;
import org.pwc.assignment.model.entity.User;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

/**
 * The implementation for this class is not provided. It is just added though, just to highlight the concept of having
 * several data access object implementations for the same contract provided by the AddressBook interface.
 * @author Nader Albert
 * @since 7/18/2014.
 */
public class AddressBookMongoDao implements AddressBookDao{
  @Override
  public boolean saveAddressBook(AddressBook book) {
    throw new NotImplementedException();
  }

  @Override
  public AddressBook retrieveAddressBook(User user) {
    throw new NotImplementedException();
  }
}
