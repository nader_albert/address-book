package org.pwc.assignment.dao;

import org.pwc.assignment.dao.exception.DataAccessException;
import org.pwc.assignment.model.entity.AddressBook;
import org.pwc.assignment.model.entity.User;

/**
 * @author Nader Albert
 * @since 7/18/2014.
 */
public interface AddressBookDao {

  /**
   * allows the client to save a newly created address book
   * @throws org.pwc.assignment.dao.exception.DataAccessException, if an unexpected problem happens while dealing with
   * the persistence backend.
   * //This function shouldn't allow an Address Book to be save twice. Hence, an AddressBook that already exists should be
   * //rejected.
   * @return false, if this AddressBook hasn't been successfully saved, and true otherwise
   * */
  public boolean saveAddressBook (AddressBook book) throws DataAccessException;

  /**
   * allows the client to update an address book that already exists. replaces the existing AddressBook
   * with the one provided as a parameter.
   * An Address Book is identified by the identity of its owner
   * @throws org.pwc.assignment.dao.exception.DataAccessException, if an unexpected problem happens while dealing with
   * the persistence backend.
   * This function shouldn't allow an Address Book to be updated if it doesn't originally exist.
   * @return false, if this AddressBook hasn't been updated successfully and true otherwise.
   * */
  //public boolean updateAddressBook(AddressBook book) throws DataAccessException;

  /***
   * retrieves the Address Book, corresponding to the user provided as a parameter
   * @throws org.pwc.assignment.dao.exception.DataAccessException, if an unexpected problem happens while dealing with
   * the persistence backend.
   * @return the designated AddressBook.
   * */
  public AddressBook retrieveAddressBook(User user) throws DataAccessException;

}
