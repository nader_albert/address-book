package org.pwc.assignment.service;

import org.junit.Test;
import org.junit.Assert;
import java.util.TreeSet;
import java.util.SortedSet;

import org.pwc.assignment.model.entity.User;
import org.pwc.assignment.model.entity.Contact;
import org.pwc.assignment.model.entity.AddressBook;
import org.pwc.assignment.model.exception.ValidationException;
import org.pwc.assignment.model.value.PhoneNumber;
import org.pwc.assignment.service.exception.BusinessException;

/**
 * @author Nader Albert
 * @since 7/18/2014.
 */
public class AddressBookServiceTest {

  private  AddressBookService service = new AddressBookService(); // should be removed, and replaced by a spring injection
  /**
   * called at the beginning of each test case, to ensure proper initialization.
   * ensures that every test case is totally independent from other ones
   * */
  private boolean initializeTest(User user){
    try {
      return service.deleteAddressBook(user);
    } catch (BusinessException e) {
      return false;
    }
  }

  @Test
  public void testAddContact() {
    User user = new User("dodo");
    Assert.assertTrue(initializeTest(user));
    try {
      try {
        Assert.assertEquals(true,service.addContact(user, new Contact("nader",new PhoneNumber("0124412120"))));
      } catch (ValidationException vex) {
        Assert.fail("a validation exception has been thrown: " + vex.getMessage());
      }
    } catch (BusinessException bex) {
      Assert.fail("a business exception has been thrown: " + bex.getMessage());
    }
  }

  @Test
  public void testDisplayFriends() {
    User userTom = new User("Tom");
    Assert.assertTrue(initializeTest(userTom));

    //composing a fake Address Book, just to compare with
    AddressBook fakeBook1 = new AddressBook(userTom);
    try {
      fakeBook1.addContact(new Contact("Josh", new PhoneNumber("0122441212")));
      fakeBook1.addContact(new Contact("Sandra", new PhoneNumber("0122441212")));
    } catch (ValidationException vex){
      Assert.fail("a validation exception has been thrown: " + vex.getMessage());
    }
    try {
      try {
        service.addContact(userTom, new Contact("Josh", new PhoneNumber("0122441203")));
        service.addContact(userTom, new Contact("Sandra", new PhoneNumber("0413464122")));
      } catch (ValidationException vex){
        Assert.fail("a validation exception has been thrown: " + vex.getMessage());
      }

      AddressBook retrievedBook1 = service.displayAddressBook(userTom);
      Assert.assertTrue(fakeBook1.equals(retrievedBook1));
      System.out.println(retrievedBook1);
    } catch (BusinessException bex) {
      System.out.println(bex.getMessage());
      Assert.fail();
    }

    User userBen = new User("Ben");
    Assert.assertTrue(initializeTest(userBen));
    AddressBook fakeBook2 = new AddressBook(userBen);

    try {
      fakeBook2.addContact(new Contact("Leo", new PhoneNumber("0122441205")));
      fakeBook2.addContact(new Contact("Soli", new PhoneNumber("0134641220")));
    } catch (ValidationException vex){
      Assert.fail("a validation exception has been thrown: " + vex.getMessage());
    }

    try {
      try {
        service.addContact(userBen, new Contact("Leo", new PhoneNumber("0122441203")));
        service.addContact(userBen, new Contact("Soli", new PhoneNumber("0134641220")));
      } catch (ValidationException vex){
        Assert.fail("a validation exception has been thrown: " + vex.getMessage());
      }

      AddressBook retrievedBook2 = service.displayAddressBook(userBen);
      Assert.assertTrue(fakeBook2.equals(retrievedBook2));
      System.out.println(retrievedBook2);
    } catch (BusinessException bex) {
      System.out.println(bex.getMessage());
      Assert.fail();
    }
  }

  /**
   * confirms tha the displayAddressBook will be throwing an exception, if it is required to display an AddressBook that
   * is not already there.
   * */
  @Test
  public void testDisplayFriendsWithoutSaving() {
    User userGareth = new User("Gareth");
    Assert.assertTrue(initializeTest(userGareth));

    try {
      service.displayAddressBook(userGareth);
      Assert.fail("display address book should have thrown an exception, as the address book required to be displayed doesn't exist");
    } catch (BusinessException bex) {
      Assert.assertTrue("an expected business exception is thrown, as the display address book was required to show " +
              "an Address Book that doesn't exist" + bex.getMessage(), true);
    }
  }

  /**
   * confirms that the friends list are always retrieved sorted by their names
   * */
  @Test
  public void testDisplayFriendsSorted() {
    User userSimone = new User("Simone");
    Assert.assertTrue(initializeTest(userSimone));

    AddressBook fakeBook2 = new AddressBook(userSimone);
    try {
      fakeBook2.addContact(new Contact("Leo", new PhoneNumber("0123441206")));
      fakeBook2.addContact(new Contact("Soli", new PhoneNumber("0134611220")));
      fakeBook2.addContact(new Contact("Akram", new PhoneNumber("0134331220")));
      fakeBook2.addContact(new Contact("Samir", new PhoneNumber("0134991220")));
      fakeBook2.addContact(new Contact("Karma", new PhoneNumber("0134881220")));
      fakeBook2.addContact(new Contact("Fahmy", new PhoneNumber("0177641220")));
    } catch (ValidationException vex){
      Assert.fail("a validation exception has been thrown: " + vex.getMessage());
    }

    try {
      service.addContact(userSimone, new Contact("Leo",new PhoneNumber("0122441208")));
      service.addContact(userSimone, new Contact("Fahmy",new PhoneNumber("0177641220")));
      service.addContact(userSimone, new Contact("Soli",new PhoneNumber("0134641220")));
      service.addContact(userSimone, new Contact("Akram",new PhoneNumber("0134331220")));
      service.addContact(userSimone, new Contact("Samir",new PhoneNumber("0134991220")));
      service.addContact(userSimone, new Contact("Karma",new PhoneNumber("0134881220")));

      AddressBook retrievedBook2 = service.displayAddressBook(userSimone);
      Assert.assertEquals(retrievedBook2, fakeBook2);
    } catch (ValidationException vex){
      Assert.fail("a validation exception has been thrown: " + vex.getMessage());
    } catch (BusinessException bex) {
        Assert.assertTrue("an expected business exception is thrown, as the display address book was required to show " +
              "an Address Book that doesn't existi" + bex.getMessage(), true);
    }
  }

  /**
   * confirms that the friends list are always saved without duplicates
   * */
  @Test
  public void testDisplayFriendsWithoutDuplicate() {
    User userRimo = new User("Rimo");
    Assert.assertTrue(initializeTest(userRimo));

    AddressBook fakeBook2 = new AddressBook(userRimo);
    try {
      fakeBook2.addContact(new Contact("Soli", new PhoneNumber("0134611220")));
      fakeBook2.addContact(new Contact("Soli", new PhoneNumber("0134611220")));
      fakeBook2.addContact(new Contact("Soli", new PhoneNumber("0134611220")));
      fakeBook2.addContact(new Contact("Soli", new PhoneNumber("0134611220")));
      fakeBook2.addContact(new Contact("Karma", new PhoneNumber("0134881220")));
      fakeBook2.addContact(new Contact("Fahmy", new PhoneNumber("0177641220")));
    } catch (ValidationException vex){
      Assert.fail("a validation exception has been thrown: " + vex.getMessage());
    }

    try {
      service.addContact(userRimo,new Contact("Soli",new PhoneNumber("0134611220")));
      service.addContact(userRimo,new Contact("Soli",new PhoneNumber("0134611220")));
      service.addContact(userRimo,new Contact("Soli",new PhoneNumber("0134611220")));
      service.addContact(userRimo,new Contact("Soli",new PhoneNumber("0134611220")));
      service.addContact(userRimo,new Contact("Karma",new PhoneNumber("0134881220")));
      service.addContact(userRimo,new Contact("Fahmy",new PhoneNumber("0177641220")));

      AddressBook retrievedBook2 = service.displayAddressBook(userRimo);
      Assert.assertTrue(3 == retrievedBook2.getContacts().size()); // only three contacts should be in the list, and the others are duplicate
    } catch (ValidationException vex){
        Assert.fail("a validation exception has been thrown: " + vex.getMessage());
    } catch (BusinessException bex) {
        Assert.fail("an exception has been thrown while attempting to display contacts in an address book" + bex.getMessage());
    }
  }

  @Test
  public void testDetectUniqueFriends() {
    User user = new User("dodo");
    SortedSet<Contact> contacts = new TreeSet<Contact>();

    Assert.assertTrue(initializeTest(user));

    AddressBook book1 = new AddressBook(new User("dodo"));
    AddressBook book2 = new AddressBook(new User("dodo"));

    try {
      book1.addContact(new Contact("Bob", new PhoneNumber("0122441320")));
      book1.addContact(new Contact("Mary", new PhoneNumber("0134641220")));
      book1.addContact(new Contact("Jane", new PhoneNumber("0134641220")));

      book2 = new AddressBook(new User("dodo"));
      book2.addContact(new Contact("Mary", new PhoneNumber("0122441207")));
      book2.addContact(new Contact("John", new PhoneNumber("0134641220")));
      book2.addContact(new Contact("Jane", new PhoneNumber("0134641220")));

      contacts.add(new Contact("Bob", new PhoneNumber("0122441204")));
      contacts.add(new Contact("John", new PhoneNumber("0122441205")));
    } catch (ValidationException vex){
      Assert.fail("a validation exception has been thrown: " + vex.getMessage());
    }
    try {
      Assert.assertEquals(contacts, service.detectUniqueFriends(book1, book2));
    } catch (BusinessException e) {
      e.printStackTrace();
    }
  }
}
