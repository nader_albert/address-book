package org.pwc.assignment.service;

/**
 * @author Nader Albert
 * @since 7/18/2014.
 */
public enum Commands {
  SHOW("show"),
  ADD("add"),
  COMPARE("diff"),
  SWITCH("switch"),
  QUIT("quit");

  private Commands(String key) {
    this.key = key;
  }

  public String toString() {
    return key;
  }

  private String key;
}
