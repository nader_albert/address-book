package org.pwc.assignment.service.exception;

/**
 * @author Nader Albert
 * @since 7/18/2014.
 */
public class BusinessException extends Exception{
  public BusinessException(String errorMessage) {
    super (errorMessage);
  }
}
