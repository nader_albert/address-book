package org.pwc.assignment.service;

import org.springframework.util.Assert;
import org.pwc.assignment.model.entity.User;
import org.pwc.assignment.model.entity.Contact;
import org.pwc.assignment.model.value.PhoneNumber;
import org.pwc.assignment.model.entity.AddressBook;
import org.pwc.assignment.service.exception.BusinessException;
import org.pwc.assignment.model.exception.ValidationException;

import java.util.SortedSet;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Provides a sample command based client application, that communicates with the AddressBook Service to add a friend
 * to an address book, show the list of friends for a certain address book, or to detect the unique lists of friends
 * between two address books.
 *
 * At first you will be required to enter a user name (owner of an address book). This will remain the active
 * Address Book owner, until you changed it explicitly through the Switch command. Being Active implies that all
 * subsequent commands like 'Add', 'Show', 'Diff', are going to be applied on this user.
 *
 * 4 commands are then made available from the command line, and you are given the option to select any of them:
 *
 *  ##Add##
 *  Allows you to add a new contact to the current active user. To add a new contact, you enter its name and its
 *  phone number. The phone number has to be 10 digits, or otherwise, it will be rejected.
 *
 *  ##Show##
 *  Displays the list of friends associated with the current active user. For each of them, the name and the phone number
 *  are being displayed.
 *
 *  ##Switch##
 *  Allows you to switch the active user from the current one to a new one. prompts you to enter the name of the new user
 *  to be active and replace the current one. From now one, all subsequent commands (add, show, diff) will be applied to
 *  this user.
 *
 * ##Diff
 * Allows you to compare the friend list of one user, and the current active user. You will be prompted to enter the
 * other user, for whom, you need to compare the friend list against those of the currently active user

 * @author Nader Albert
 * @since 7/18/2014.
 */
public class AddressBookApp {
  static AddressBookService service = new AddressBookService();
  static String newCommandMessage =
          "\n************************************ \n#### PLEASE ENTER A COMMAND ####\n"
          + "************************************\n";
  public static void main (String args[]){
    System.out.print("Enter Address Book owner name: ");

    String userName = readMessage();
    Assert.notNull(userName);

    System.out.println("Now Enter the command you need to run: options are:" +" \n" +
            " [show] :: displays the address book associated with the user name just entered " + " \n" +
            " [add] :: adds a new contact to the address book associated with the user name just entered " + "\n" +
            " [diff] :: display the list of friends that are unique to each address book " + " \n" +
            " [switch] :: to switch to a different address book user " + " \n" +
            " [quit] :: to exit the application");

    readComand(userName);
  }

  /**
   * */
  private static Commands readComand(String userName) {
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    String command = null;

    try {
      command = br.readLine();
    } catch (IOException ioe) {
      System.out.println("IO error trying to read your name!");
    }

    switch (command) {
      case "add":
        handleAddCommand(userName);
        readComand(userName);
      case "show":
        handleShowCommand(userName);
        readComand(userName);
      case "diff":
        handleCompareCommand(userName);
        readComand(userName);
      case "switch":
        String newUserName = handleSwitchCommand();
        System.out.println("contact successfully added \n" + newCommandMessage );
        readComand(newUserName);
      case "quit": {
        System.out.println("Thank you for using this application!");
        System.exit(1);
      }
    }
    System.out.println("invalid command entered... please try again!");
    return readComand(userName);
  }

  /**
   * */
  private static void handleAddCommand(String userName){
    System.out.print("You are now required to enter the contact details : \n");

    System.out.print("Enter Contact Name: \n");
    String contactName = readMessage();

    System.out.print("Enter Contact Number (10 digits): \n");
    String contactNumber = readMessage();

    try {
      Contact contact = new Contact(contactName,new PhoneNumber(contactNumber));
      service.addContact(new User(userName),contact);
      System.out.println("contact successfully added \n" + newCommandMessage );
    } catch (ValidationException vex) {
      System.out.println(vex.getMessage() + " contact not added " + newCommandMessage);
    } catch (BusinessException bex) {
      System.out.println("couldn't add this contact due to the following error: " + bex.getMessage());
    }
  }

  /**
   * */
  private static String handleSwitchCommand(){
    System.out.print("Enter the new user name: " + " \n");
    String newUser = readMessage();
    System.out.print("Current Active User is set to: " + newUser + " \n");
    return newUser;
  }

  /**
   * */
  private static void handleShowCommand(String userName){
    System.out.print("Contact details for user: "+ userName + " are: " + " \n");
    try {
      AddressBook book = service.displayAddressBook(new User(userName));
      for (Contact contact : book.getContacts())
        System.out.print(contact + " \n");

      System.out.println(newCommandMessage);
    } catch (BusinessException bex) {
      System.out.println("No Address Book exists for this user ");
      System.out.println(newCommandMessage);
    }
  }

  /**
   * */
  private static void handleCompareCommand(String userName){
    AddressBook book1= null;
    AddressBook book2= null;

    System.out.print("Please enter the user name, who owns the address book, that you wish to compare with the current "
            + "active user" + " \n");

    System.out.print("Enter User Name: \n");
    String otherUserName = readMessage();

    System.out.print("The address book of user: " + otherUserName + " will now be compared with the current active user: "
            + userName +" \n");

    try {
      book1 = service.displayAddressBook(new User(userName));
      book2 = service.displayAddressBook(new User(otherUserName));
    } catch (BusinessException bex){
      System.out.println("Couldn't retrieve one of the Address Books. Comparison won't be possible ");
      System.out.println(newCommandMessage);
    }

    try {
      SortedSet<Contact> uniqueFriends = service.detectUniqueFriends(book1,book2);
      if (0 == uniqueFriends.size())
        System.out.print("No unique friends have been detected between the address book of [" + userName + "] and ["
                + otherUserName + "]" + " \n");
      else
        System.out.print("The following friends have been detected to be unique to both address books " + " \n");
      for (Contact friend : uniqueFriends)
        System.out.print(friend + " \n");
    } catch (BusinessException bex) {
      System.out.println("address book couldn't be retrieved, due to teh following error: " + bex.getMessage());
      System.out.println(newCommandMessage);
    }
  }

  /**
   * */
  private static String readMessage(){
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    String inputString = null;

    try {
      inputString = br.readLine();
    } catch (IOException ioe) {
      System.out.println("IO error trying to read your name!");
      System.exit(1);
    }
    return inputString;
  }
}
