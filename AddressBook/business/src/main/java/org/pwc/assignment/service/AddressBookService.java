package org.pwc.assignment.service;

import org.pwc.assignment.model.entity.User;
import org.pwc.assignment.model.entity.Contact;
import org.pwc.assignment.model.entity.AddressBook;
import org.pwc.assignment.dao.impl.AddressBookFileSystemDao;
import org.pwc.assignment.dao.exception.DataAccessException;
import org.pwc.assignment.service.exception.BusinessException;

import java.util.SortedSet;
import java.util.TreeSet;

/**
 * Performs all business logic related to the Address Book, as per the use case described in the assignment.
 * @author Nader Albert
 * @since 7/18/2014.
 */
public class AddressBookService {
  //Spring could have been used to ensure proper dependency injection, and a singleton instance from the Dao and
  //the service to be available, instead of creating new instance from the dao in the AddressBook Service.
  //This is not how it should be anyway. Instead, a single stateless Service instance and dao should be made available
  //to the entire application
  private AddressBookFileSystemDao dao = new AddressBookFileSystemDao();
  /**
   * adds a new friend to the list of friends, that a given user does possess
   * @param user, represents the user who owns the address book, required to be altered
   * @param contact, represents the contact information required to be added to the identified address book.
   * @return true if the contact is added successfully to the intended address book, false otherwise
   * */
  public boolean addContact (User user, Contact contact) throws BusinessException{
    AddressBook book;

    if (dao.exists(user))
      try {
        book = dao.retrieveAddressBook(user);
      } catch (DataAccessException dex) {
        throw new BusinessException("backend storage not ready for read activity, because of the following error: "
                + dex.getMessage());
      }
    else book = new AddressBook(user);

    book.addContact(contact);

    try {
      dao.saveAddressBook(book);
    } catch (DataAccessException dex) {
      throw new BusinessException("backend storage not ready for writing activity, because of the following error: "
              + dex.getMessage());
    }
    return true;
  }

  /**
   * displays the list of friends associated with a given user.
   * @param user, represents the user who owns the address book, required to be altered
   * @return, the address book, corresponding to the user given as a parameter
   * */
  public AddressBook displayAddressBook (User user) throws BusinessException{
    AddressBook book;
    try {
      book = dao.retrieveAddressBook(user);
    } catch (DataAccessException dex) {
      throw new BusinessException(" backend storage not ready for address book display..: "
              + dex.getMessage());
    }
    return book;
  }

  /**
   * @param user, the user whose Address book will be deleted
   * @return true if the deletion was successful, false otherwise
   * */
  public boolean deleteAddressBook(User user) throws BusinessException{
    boolean deletionStatus;
    try {
      deletionStatus = dao.deleteAddressBook(user);
    } catch (DataAccessException dex) {
      throw new BusinessException("couldn't delete existing address book due to the following error: "
              + dex.getMessage());
    }
    return deletionStatus;
  }

  /**
   * computes the union of the relative complements between two sets of contacts belonging to two address books
   * @param firstBook,the first address book, whose contacts will be compared with the second
   * @param secondBook, the second address book, whose contacts will be compared with the first
   * @return the computed union of the two contact lists.
   * */
  public SortedSet<Contact> detectUniqueFriends (AddressBook firstBook, AddressBook secondBook)
          throws BusinessException{
    TreeSet tempContacts = new TreeSet();

    tempContacts.addAll(firstBook.getContacts());

    firstBook.getContacts().removeAll(secondBook.getContacts());
    secondBook.getContacts().removeAll(tempContacts);

    TreeSet assymetricDifferenceSet = new TreeSet();
    assymetricDifferenceSet.addAll(firstBook.getContacts());
    assymetricDifferenceSet.addAll(secondBook.getContacts());

    return assymetricDifferenceSet;
  }
}
