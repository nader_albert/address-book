package org.pwc.assignment.model.exception;

/**
 * @author Nader Albert
 * @since 7/18/2014.
 */
public class ValidationException extends Exception{
  public ValidationException(String errorMessage) {
    super (errorMessage);
  }
}
