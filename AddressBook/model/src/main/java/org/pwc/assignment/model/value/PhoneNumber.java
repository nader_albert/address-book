package org.pwc.assignment.model.value;

import org.pwc.assignment.model.exception.ValidationException;

import java.io.Serializable;

/**
 * @author Nader Albert
 * @since 7/18/2014.
 */
public class PhoneNumber implements StringValueObject, Serializable {
  private String number;

  public PhoneNumber(String number) throws ValidationException {
    this.number = number;
    if (!validate())
      throw new ValidationException("invalid phone number");
  }

  @Override
  public String value() {
    return number;
  }

  @Override
  public String toString() {
    return number;
  }


  @Override
  public boolean validate() {
    //validate phone numbers of format "1234567890"
    //return number.matches("\\d{10}");
    return number.matches("\\d{10}");
  }
}
