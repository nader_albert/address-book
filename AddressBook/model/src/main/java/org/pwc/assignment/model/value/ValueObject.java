package org.pwc.assignment.model.value;


/**
 * @author Nader Albert
 * @since 7/17/2014.
 */
public interface ValueObject {
  public boolean validate();
}
