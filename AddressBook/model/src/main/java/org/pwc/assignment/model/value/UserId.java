package org.pwc.assignment.model.value;

/**
 * @author Nader Albert
 * @since 7/17/2014.
 */
public class UserId implements IntegerValueObject{
  private Integer id;

  @Override
  public Integer value() {
    return id;
  }

  /**
   * validates the identifier value of a user. assumes that the identifier value cannot exceed 1000
   * */
  @Override
  public boolean validate() {
    if (id > 0 && id < 1000)
      return true;
    else return false;
  }
}
