package org.pwc.assignment.model.value;

/**
 * @author Nader Albert
 * @since 7/18/2014.
 */
public interface StringValueObject extends ValueObject{
  public String value();


}
