package org.pwc.assignment.model.value;


/**
 * @author Nader Albert
 * @since 7/18/2014.
 */
public interface IntegerValueObject extends ValueObject{
  public Integer value();

}
