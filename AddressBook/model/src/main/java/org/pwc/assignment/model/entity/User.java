package org.pwc.assignment.model.entity;

import org.springframework.util.Assert;

import java.io.Serializable;

/**
 * @author Nader Albert
 * @since 7/18/2014.
 */
public class User implements Serializable {
  private String name;

  public String getName() {
    return name;
  }

  public User(String userName){
    this.name = userName;
  }

  @Override
  public String toString(){
    return this.name;
  }

  @Override
  public boolean equals (Object otherUser){
    Assert.isInstanceOf(User.class, otherUser);
    return this.name.equals(((User)otherUser).name);
  }
}
