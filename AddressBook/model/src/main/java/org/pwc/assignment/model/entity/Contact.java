package org.pwc.assignment.model.entity;

import org.pwc.assignment.model.value.PhoneNumber;
import org.springframework.util.Assert;
import java.io.Serializable;

/**
 * @author Nader Albert
 * @since 7/17/2014.
 */
public class Contact implements Comparable, Serializable {
  private String name;
  private PhoneNumber number;

  public Contact(String name, PhoneNumber phoneNumber) {
    this.name = name;
    this.number = phoneNumber;
  }

  public String getName(){
    return name;
  }

  @Override
  public String toString(){
    return "[contact information] " + "name: " + this.name + " || " + "phone: " + this.number;
  }

  /**
   * the phone number is left intentionally, as contacts should be deemed equal based on their name, irrespective of
   * the number as two Contact with the same name but with different phone numbers should not be allowed, and thus
   * should not be considered unequal.
   * */
  @Override
  public boolean equals (Object otherContact){
    Assert.isInstanceOf(Contact.class,otherContact);
    return this.getName().equals(((Contact)otherContact).getName());
  }

  /**
   * list of contacts should be sorted by their names
   * */
  @Override
  public int compareTo(Object otherContact) {
    Assert.isInstanceOf(Contact.class,otherContact);
    return this.name.compareTo(((Contact)otherContact).getName());
  }
}
