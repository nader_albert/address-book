package org.pwc.assignment.model.entity;

import org.springframework.util.Assert;

import java.io.Serializable;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * @author Nader Albert
 * @since 7/18/2014.
 */
public class AddressBook implements Serializable{
  private User owner; // This user represents the owner of this address book, which differentiates this address book from another
  private TreeSet<Contact> contacts;

  /**
   * an address book cannot be created without an owner.
   * however it can be created with empty contact list.
   * */
  public AddressBook(User owner) {
    this.owner = owner;
  }

  public User getOwner() {
    return owner;
  }

  public void addContact(Contact contact){
    if(null == contacts)
      contacts = new TreeSet<Contact>();
    contacts.add(contact);
  }

  public void removeContact(Contact contact){
    contacts.remove(contact);
  }

  public TreeSet<Contact> getContacts(){
    return contacts;
  }

  @Override
  public String toString(){
    return "** Address Book Owner: **" + this.owner + " || " + "** Contacts: **: " + this.contacts;
  }

  @Override
  public boolean equals (Object otherAddressBook){
    Assert.isInstanceOf(AddressBook.class, otherAddressBook);
    User otherOwner = ((AddressBook)otherAddressBook).getOwner();
    SortedSet otherContacts = ((AddressBook)otherAddressBook).getContacts();
    return
            (
              this.getOwner().equals(otherOwner)
            &&
              this.getContacts().equals(otherContacts)
            );
  }


}
